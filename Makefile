##
## Makefile for make in /home/amstut_a/rendu/piscine_cpp_d01/ex01
## 
## Made by amstut_a
## Login   <amstut_a@epitech.net>
## 
## Started on  Wed Jan  7 11:14:43 2015 amstut_a
## Last update Sat Jan 17 18:58:19 2015 Arthur Amstutz
##

SRC	= main.cpp \
	  UnitTests.cpp \
	  Object.cpp \
	  Toy.cpp \
	  Teddy.cpp \
	  LittlePony.cpp \
	  Wrap.cpp \
	  Box.cpp \
	  GiftPaper.cpp \
	  Table.cpp \
	  TableRand.cpp \
	  IElf.cpp \
	  ElfOfPePeNoel.cpp \
	  ConveyorBelt.cpp

CXXFLAGS	= -W -Wall -Wextra -Werror

OBJ		= $(SRC:.cpp=.o)

NAME		= UnitTests

CC		= g++

RM		= rm -f

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
