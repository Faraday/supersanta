//
// Toy.hh for toy in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 10:32:21 2015 Arthur Amstutz
// Last update Sat Jan 17 17:34:25 2015 Frédéric Ledarath
//

#ifndef TOY_HH_
# define TOY_HH_

#include "Object.hh"

class Toy: public Object
{
public:
  Toy(const std::string &t);
  virtual	~Toy();
  virtual void 	isTaken() const = 0;
};

#endif
