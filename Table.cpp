//
// Table.cpp for qdsqdq in /home/ledara_f/rendu/rush2
// 
// Made by Frédéric Ledarath
// Login   <ledara_f@epitech.net>
// 
// Started on  Sat Jan 17 13:25:29 2015 Frédéric Ledarath
// Last update Sat Jan 17 20:30:52 2015 Frédéric Ledarath
//

#include <iostream>
#include "Table.hh"

TablePePeNoel::TablePePeNoel()
{
  int	i = 0;
  while (i < 10)
    {
      this->s[i] = NULL;
      i++;
    }
  this->nb = 0;
}

TablePePeNoel::~TablePePeNoel()
{
}

void TablePePeNoel::Put(Object *s)
{
  int	i = 0;
  while (this->s[i] != NULL)
    i++;
  if (i < 10)
    {
      this->s[i] = s;
      this->nb += 1;
      return ;
    }
  std::cout << "Error already 10 object in table" << std::endl;
}

std::string **TablePePeNoel::Look()
{
  int	i = 0;
  int	j = 0;
  std::string **lol = new std::string*[this->nb + 1];

  while (i < this->nb)
    {
      if (this->s[i] != NULL)
	{
	  lol[j] = this->s[i]->getTitle();
	  j++;
	}
      i++;
    }
  lol[i] = NULL;
  return lol;
}

Object*	TablePePeNoel::Take(int idx)
{

  int	i = 0;
  if (idx > 9 || idx < 0)
    return NULL;
  while (i < 10)
    {
      if (i == idx)
	{
	  Object *res = this->s[i];
	  this->s[i] = NULL;
	  this->nb -= 1;
	  return res;
	}
      i++;
    }
  return NULL;
}

ITable	*TablePePeNoel::createTable()
{
  TablePePeNoel *res = new TablePePeNoel();
  return res;
}

Box	*TablePePeNoel::getBox()
{
  int	i = 0;
  while (i < 10)
    {
      if (this->s[i] != NULL)
	{
	  if (*s[i]->getTitle() == "Box")
	    return (Box *)this->Take(i);
	}
      ++i;
    }
  return NULL;
}

Toy	*TablePePeNoel::getToy()
{
  int	i = 0;
  while (i < 10)
    {
      if (this->s[i] != NULL)
	{
	  if (*(s[i])->getTitle() != "Box" && *(s[i])->getTitle() != "GiftPaper")
	    return (Toy *)this->Take(i);
	}
      ++i;
    }
  return NULL;
}

GiftPaper	*TablePePeNoel::getGiftPaper()
{
  int	i = 0;
  while (i < 10)
    {
      if (this->s[i] != NULL)
	{
	  if (*s[i]->getTitle() == "GiftPaper")
	    return ((GiftPaper *)this->Take(i));
	}
      ++i;
    }
  return NULL;
}
