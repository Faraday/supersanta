
//
// IElf.cpp for IELF in /home/amstut_a/rendu/Rush2/chap5
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 14:37:58 2015 Arthur Amstutz
// Last update Sat Jan 17 18:29:20 2015 Ludovic DOUZIECH
// Last update Sat Jan 17 18:11:32 2015 Frédéric Ledarath
//

#include "IElf.hh"

IElf::IElf(ITable *t, IConveyorBelt *cv) :
  table(t),
  cb(cv)
{
}

IElf::~IElf()
{
}
