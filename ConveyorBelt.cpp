//
// ConveyorBelt.cpp for conveyorbelt in /home/elkaim_r/projects/supersanta/chap4
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Sat Jan 17 16:44:36 2015 raphael elkaim
// Last update Sat Jan 17 18:59:19 2015 Arthur Amstutz
//

#include "ConveyorBelt.hh"

ConveyorBeltPePeNoel::ConveyorBeltPePeNoel()
{
  obj = 0;
}

ConveyorBeltPePeNoel::~ConveyorBeltPePeNoel()
{
}

void ConveyorBeltPePeNoel::Put(Object *ob)
{
  if (!obj)
    obj = ob;
  else
    std::cerr << "error:there is already something on the conveyer belt,so you can't add another object on it" << std::endl;
}

Object *ConveyorBeltPePeNoel::Take()
{
  if (!obj)
    std::cerr << "there is nothing on the conveyer belt to be taken" << std::endl;
  else
    {
      Object *res = obj;
      obj = 0;
      return res;
    }
  return NULL;
}

void ConveyorBeltPePeNoel::OUT()
{
  obj = NULL;
}

Wrap *ConveyorBeltPePeNoel::IN()
{
  if (random() % 2)
    return new GiftPaper;
  else
    return new Box;
  //  return random() % 2? new GiftPaper : new Box ;
}
