//
// Teddy.cpp for teddy in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 10:46:54 2015 Arthur Amstutz
// Last update Sat Jan 17 10:53:42 2015 Arthur Amstutz
//

#include "Teddy.hh" 
#include <iostream>

Teddy::Teddy(const std::string &t):
  Toy::Toy(t)
{
}

Teddy::~Teddy()
{
}

void Teddy::isTaken() const
{
  std::cout << "gra hu" << std::endl;
}
