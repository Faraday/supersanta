//
// UnitTests.hh for unittests in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 11:19:56 2015 Arthur Amstutz
// Last update Sat Jan 17 18:55:32 2015 Arthur Amstutz
//

#ifndef UNITTESTS_HH_
# define UNITTESTS_HH_

#include "Object.hh"

Object	**MyUnitTests();
Object	*MyUnitTests(Object **o);
void	testTable();
void	testTableRand();
void	testElf();

#endif
