//
// GiftPaper.hh for giftpaper in /home/amstut_a/rendu/Rush2/chap3
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 12:35:53 2015 Arthur Amstutz
// Last update Sat Jan 17 17:55:45 2015 Arthur Amstutz
//

#ifndef GIFTPAPER_HH_
# define GIFTPAPER_HH_

#include "Wrap.hh"

class GiftPaper: public Wrap
{
public:
  GiftPaper();
  ~GiftPaper();

  void	wrapMeThat(Object *o);
};

#endif
