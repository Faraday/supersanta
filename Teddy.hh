//
// Teddy.hh for teddy in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 10:47:08 2015 Arthur Amstutz
// Last update Sat Jan 17 10:53:05 2015 Arthur Amstutz
//

#ifndef TEDDY_HH_
# define TEDDY_HH_

#include "Toy.hh"

class Teddy: public Toy
{
public:
  Teddy(const std::string &t);
  ~Teddy();

  void	isTaken() const;
};

#endif
