//
// TableRand.cpp for qdsqsds in /home/ledara_f/divers/supersanta/chap6
// 
// Made by Frédéric Ledarath
// Login   <ledara_f@epitech.net>
// 
// Started on  Sat Jan 17 17:16:07 2015 Frédéric Ledarath
// Last update Sat Jan 17 19:43:06 2015 Frédéric Ledarath
//

#include <cstdlib>
#include <ctime>
#include "TableRand.hh"
#include "Box.hh"
#include "Teddy.hh"
#include "LittlePony.hh"
#include "GiftPaper.hh"

TableRand::TableRand():
  TablePePeNoel::TablePePeNoel()
{
  int	i = 0;
  int	rand = 0;

  while (i < 10)
    {
      rand = random() % 4;
      if (rand == 0)
	s[i] = new Box();
      else if (rand == 1)
	s[i] = new GiftPaper();
      else if (rand == 2)
	s[i] = new Teddy("Teddy");
      else
	s[i] = new LittlePony("Pony");
      i++;
    }
  this->nb = 10;
}
