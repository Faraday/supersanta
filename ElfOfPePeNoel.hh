//
// IElf.hh for IElf in /home/amstut_a/rendu/Rush2/chap5
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 14:38:10 2015 Arthur Amstutz
// Last update Sat Jan 17 17:48:42 2015 Arthur Amstutz
//

#ifndef ELFOFPEPENOEL_HH_
# define ELFOFPEPENOEL_HH_

#include "IElf.hh"

class ElfOfPePeNoel: public IElf
{
protected:
  Toy		*getToy();
  Box		*getBox();
  GiftPaper	*getGiftPaper();

public:
  ElfOfPePeNoel(ITable *t, IConveyorBelt *c);
  ~ElfOfPePeNoel();

  void		startWrapping();
  void		lookTable() const;
  void		openWrap(Wrap *wrap);
  Object	*accessWrap(Box *box);
  void		setOnTable(Object *obj);
  Object	*getFromTable(int idx) const;
};

#endif
