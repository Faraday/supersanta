//
// Wrap.cpp for wrap in /home/amstut_a/rendu/Rush2/chap3
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 11:48:31 2015 Arthur Amstutz
// Last update Sat Jan 17 16:15:35 2015 Frédéric Ledarath
//

#include "Wrap.hh"
#include <iostream>

Wrap::Wrap(const std::string &name):
  Object::Object(name),
  obj(NULL),
  isOpen(false)
{
}

Wrap::~Wrap()
{
}

void Wrap::isTaken() const
{
}

void Wrap::openMe()
{
  this->isOpen = true;
}

Object *Wrap::takeMe()
{
  Object *res = this->obj;
  
  if (this->isOpen == true)
    {
      this->obj = NULL;
      return res;
    }
  std::cerr << "Wrap is closed: can't take object" << std::endl;
  
  return NULL;
}
