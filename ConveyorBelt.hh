//
// ConveyorBelt.hh for conveyorbelt in /home/elkaim_r/rendu/cpp_santa
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Sat Jan 17 13:14:14 2015 raphael elkaim
// Last update Sat Jan 17 18:37:15 2015 Arthur Amstutz
//

#ifndef CONVEYORBELT_HH_
# define CONVEYORBELT_HH_

#include <string>
#include <iostream>
#include <cstdlib>
#include "Wrap.hh"
#include "Object.hh"
#include "GiftPaper.hh"
#include "Box.hh"
#include "IConveyorBelt.hh"

class ConveyorBeltPePeNoel : public IConveyorBelt
{
public:
  ConveyorBeltPePeNoel();
  ~ConveyorBeltPePeNoel();
  void Put(Object *);
  Object *Take();
  void OUT();
  Wrap *IN();

protected:
  Object *obj;
};

#endif
