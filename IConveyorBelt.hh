//
// IConveyorBelt.hh for iconveyorbelt in /home/amstut_a/rendu/supersanta
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 18:36:52 2015 Arthur Amstutz
// Last update Sat Jan 17 18:37:43 2015 Arthur Amstutz
//

#ifndef ICONVEYORBELT_HH_
# define ICONVEYORBELT_HH_

class IConveyorBelt
{
public:
  virtual ~IConveyorBelt(){}
  virtual void Put(Object *) = 0;
  virtual Object *Take() = 0;
  virtual void OUT() = 0;
  virtual Wrap *IN() = 0;

};

#endif
