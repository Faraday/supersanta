#ifndef ITABLE_HH_
# define ITABLE_HH_

#include <cstdlib>
#include "Object.hh"
#include "Box.hh"
#include "Toy.hh"
#include "GiftPaper.hh"

class ITable
{
protected:
  Object *s[10];
  int	nb;
public:
  virtual ~ITable(){}
  virtual std::string **Look() = 0;
  virtual void Put(Object *) = 0;
  virtual Object* Take(int idx) = 0;
  virtual ITable	*createTable() = 0;
  virtual Box		*getBox() = 0;
  virtual Toy		*getToy() = 0;
  virtual GiftPaper	*getGiftPaper() = 0;
};

#endif
