//
// Object.cpp for object in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 10:27:49 2015 Arthur Amstutz
// Last update Sat Jan 17 15:30:25 2015 Frédéric Ledarath
//

#include "Object.hh"


Object::Object(const std::string &t):
  title(t)
{
}

Object::~Object()
{
}

std::string *Object::getTitle()
{
  return (&this->title);
}
