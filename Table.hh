//
// Table.hh for qsdqs in /home/ledara_f/rendu/rush2
// 
// Made by Frédéric Ledarath
// Login   <ledara_f@epitech.net>
// 
// Started on  Sat Jan 17 12:59:20 2015 Frédéric Ledarath
// Last update Sat Jan 17 18:43:54 2015 Frédéric Ledarath
//

#ifndef TABLE_HH_
# define TABLE_HH_

#include <cstdlib>
#include "Object.hh"
#include "Box.hh"
#include "Toy.hh"
#include "GiftPaper.hh"
#include "ITable.hh"

class TablePePeNoel : public ITable
{
public:
  TablePePeNoel();
  ~TablePePeNoel();
  std::string **Look();
  void Put(Object *);
  Object* Take(int idx);
  ITable	*createTable();
  Box		*getBox();
  Toy		*getToy();
  GiftPaper	*getGiftPaper();
};

#endif 
