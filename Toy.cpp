//
// Toy.cpp for toy in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 10:32:11 2015 Arthur Amstutz
// Last update Sat Jan 17 11:33:45 2015 Arthur Amstutz
//

#include "Toy.hh"
#include <iostream>

Toy::Toy(const std::string &t):
  Object::Object(t)
{
}

Toy::~Toy()
{
}
