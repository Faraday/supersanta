//
// Box.hh for box in /home/amstut_a/rendu/Rush2/chap3
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 11:59:14 2015 Arthur Amstutz
// Last update Sat Jan 17 16:22:30 2015 Frédéric Ledarath
//

#ifndef BOX_HH_
# define BOX_HH_

#include "Wrap.hh"

class Box: public Wrap
{
public:
  Box();
  ~Box();

  void	wrapMeThat(Object *o);
  void	closeMe();
  bool	isOpened() const;
};

#endif
