//
// Wrap.hh for wrap in /home/amstut_a/rendu/Rush2/chap3
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 11:48:47 2015 Arthur Amstutz
// Last update Sat Jan 17 17:34:34 2015 Frédéric Ledarath
//

#ifndef WRAP_HH_
# define WRAP_HH_

#include "Object.hh"

class Wrap: public Object
{
protected:
  Object	*obj;
  bool		isOpen;

public:
  Wrap(const std::string &);
  virtual ~Wrap();
  virtual void	wrapMeThat(Object *o) = 0;
  void		isTaken() const;
  void		openMe();
  Object	*takeMe();
};

#endif
