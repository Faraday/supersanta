//
// IElf.hh for IElf in /home/amstut_a/rendu/Rush2/chap5
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 14:38:10 2015 Arthur Amstutz
// Last update Sat Jan 17 18:39:55 2015 Ludovic DOUZIECH
//

#ifndef IELF_HH_
# define IELF_HH_

#include "Object.hh"
#include "ITable.hh"
#include "IConveyorBelt.hh"
#include "Wrap.hh"
#include "Toy.hh"
#include "Box.hh"
#include "GiftPaper.hh"

class IElf
{
protected:
  ITable        *table;
  IConveyorBelt	*cb;

  virtual Toy		*getToy() = 0;
  virtual Box		*getBox() = 0;
  virtual GiftPaper	*getGiftPaper() = 0;


public:
  IElf(ITable *, IConveyorBelt *);
  virtual	~IElf();

  virtual void startWrapping() = 0;
  virtual void lookTable() const = 0;
  virtual void openWrap(Wrap *) = 0;
  virtual Object *accessWrap(Box *) = 0;
  virtual void setOnTable(Object *) = 0;
  virtual Object *getFromTable(int) const = 0;
};

#endif
