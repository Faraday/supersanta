//
// UnitTests.cpp for unittests in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 11:16:50 2015 Arthur Amstutz
// Last update Sat Jan 17 19:13:27 2015 Arthur Amstutz
//

#include <iostream>
#include "UnitTests.hh"
#include "Teddy.hh"
#include "LittlePony.hh"
#include "Box.hh"
#include "GiftPaper.hh"
#include "Table.hh"
#include "TableRand.hh"
#include "ElfOfPePeNoel.hh"
#include "ConveyorBelt.hh"

Object	**MyUnitTests()
{
  Object **res = new Object*[2];
  
  res[0] = new LittlePony("gay pony");
  res[1] = new Teddy("bisounours");

  return res;
}

Object	*MyUnitTests(Object ** o)
{
  ((Box*)(o[1]))->openMe();
  ((Box*)(o[1]))->wrapMeThat(o[0]);
  ((Box*)(o[1]))->closeMe();

  ((GiftPaper*)o[2])->wrapMeThat(o[1]);

  return o[2];
}

void	testTable()
{
  TablePePeNoel t;
  Teddy lol1("rush");
  Teddy lol2("de");
  Teddy lol3("merde");

  t.Put(&lol1);
  t.Put(&lol2);
  t.Put(&lol3);
  std::string **lol = t.Look();
  int	i = 0;
  std::cout << "---------- TEST TABLE -----------" << std::endl;
  while (lol[i] != NULL)
    {
      std::cout << *(lol)[i] << std::endl;
      i++;
    }
}

void	testTableRand()
{
  int	 i = 0;
  std::cout << "------------- TEST TABLE RAND --------- " << std::endl;
  TableRand p;
  std::string **lol = p.Look();
  i = 0;
  while (lol[i] != NULL)
    {
      std::cout << *(lol)[i] << std::endl;
      i++;
    }
}

void	testElf()
{
  std::cout << "------------- TEST ELF WRAPPING ------------" << std::endl;

  int i = 0;
  TableRand p;
  ConveyorBeltPePeNoel c;
  std::string **a = p.Look();
  
  while (a[i] != NULL)
    {
      std::cout << *(a)[i] << std::endl;
      ++i;
    }

  std::cout << "------ Start Wrapping -------" << std::endl;

  ElfOfPePeNoel	b(&p, &c);

  b.startWrapping();
}
