//
// LittlePony.hh for littlepony in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 10:37:49 2015 Arthur Amstutz
// Last update Sat Jan 17 10:43:04 2015 Arthur Amstutz
//

#ifndef LITTLEPONY_HH_
# define LITTLEPONY_HH_

#include "Toy.hh"

class LittlePony: public Toy
{
public:
  LittlePony(const std::string &t);
  ~LittlePony();

  void	isTaken() const;
};

#endif
