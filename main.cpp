#include "UnitTests.hh"
#include "Teddy.hh"
#include "Box.hh"
#include "GiftPaper.hh"
#include "Table.hh"
#include "TableRand.hh"
#include <iostream>

int main()
{
  Object **a = MyUnitTests();

  a[0]->isTaken();
  a[1]->isTaken();

  std::cout << "------------------------" << std::endl;

  Object **b = new Object*[4];

  b[0] = new Teddy("enfoire");
  b[1] = new Box;
  b[2] = new GiftPaper;
  b[3] = NULL;

  Object *c = MyUnitTests(b);

  ((GiftPaper*)c)->wrapMeThat(a[1]);


  srandom(time(0));
  testElf();
}
