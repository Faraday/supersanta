//
// GiftPaper.cpp for giftpaper in /home/amstut_a/rendu/Rush2/chap3
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 12:35:36 2015 Arthur Amstutz
// Last update Sat Jan 17 17:55:31 2015 Arthur Amstutz
//

#include <iostream>
#include "GiftPaper.hh"

GiftPaper::GiftPaper():
  Wrap::Wrap("GiftPaper")
{
}

GiftPaper::~GiftPaper()
{
}

void GiftPaper::wrapMeThat(Object *o)
{
  if (obj != NULL)
    std::cerr << "Can't wrap: already an object here" << std::endl;
  else
    this->obj = o;
}
