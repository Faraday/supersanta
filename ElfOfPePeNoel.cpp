//
// IElf.cpp for IELF in /home/amstut_a/rendu/Rush2/chap5
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 14:37:58 2015 Arthur Amstutz
// Last update Sat Jan 17 20:33:09 2015 Frédéric Ledarath
//

#include "ElfOfPePeNoel.hh"
#include <iostream>

ElfOfPePeNoel::ElfOfPePeNoel(ITable *t, IConveyorBelt *c):
  IElf::IElf(t, c)
{
}

ElfOfPePeNoel::~ElfOfPePeNoel()
{
}

void ElfOfPePeNoel::openWrap(Wrap *wrap)
{
  wrap->openMe();
}

Object *ElfOfPePeNoel::accessWrap(Box *box)
{
  return box->takeMe();
}

void ElfOfPePeNoel::setOnTable(Object *obj)
{
  this->table->Put(obj);
}

Object *ElfOfPePeNoel::getFromTable(int idx) const
{
  return this->table->Take(idx);
}

void ElfOfPePeNoel::lookTable() const
{
  int i = 0;
  std::string **tmp = this->table->Look();

  if (tmp == NULL)
    return ;
  while (tmp[i] != NULL)
    {
      if (tmp[i] != NULL)
	std::cout << "[" << i << "]  " << *(tmp)[i] << std::endl;
      ++i;
    }
}

Toy *ElfOfPePeNoel::getToy()
{
  return this->table->getToy();
}

Box *ElfOfPePeNoel::getBox()
{
  Box *res = this->table->getBox();

  if (res != NULL)
    return res;
  res = (Box*)this->cb->IN();
  if (*(res->getTitle()) == "Box")
    {
      std::cout << "whistles while working" << std::endl;
      return res;
    }
 
  return NULL;
}

GiftPaper *ElfOfPePeNoel::getGiftPaper()
{
  GiftPaper *res = this->table->getGiftPaper();

  if (res != NULL)
    return res;
  res = (GiftPaper*)this->cb->IN();
  if (*(res->getTitle()) == "GiftPaper")
    {
      std::cout << "whistles while working" << std::endl;
      return res;
    }
 
  return NULL;
}

void ElfOfPePeNoel::startWrapping()
{
  while (1)
    {
      Toy		*toy = this->getToy();
      Box		*box = this->getBox();
      GiftPaper		*paper = this->getGiftPaper();

      if (toy == NULL || box == NULL || paper == NULL)
	break;

      box->openMe();
      box->wrapMeThat(toy);
      paper->wrapMeThat(box);
      this->cb->Put(paper);
      this->cb->OUT();

      std::cout << "tuuuut tuuut tuut" << std::endl;
    }
  std::cout << "pepe ya un schmolle dans le bignou" << std::endl;
}
