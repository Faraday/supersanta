//
// Box.cpp for box in /home/amstut_a/rendu/Rush2/chap3
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 11:59:05 2015 Arthur Amstutz
// Last update Sat Jan 17 16:22:37 2015 Frédéric Ledarath
//

#include "Box.hh"
#include <iostream>

Box::Box():
  Wrap::Wrap("Box")
{
}

Box::~Box()
{
}

void Box::wrapMeThat(Object *o)
{
  if (this->obj == NULL && this->isOpen == true)
    this->obj = o;
  else
    std::cerr << "Can't wrap: box not opened" << std::endl;
}

void Box::closeMe()
{
  this->isOpen = false;
}

bool Box::isOpened() const
{
  return this->isOpen;
}
