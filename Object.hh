//
// Object.hh for object in /home/amstut_a/rendu/Rush2
// 
// Made by Arthur Amstutz
// Login   <amstut_a@epitech.net>
// 
// Started on  Sat Jan 17 10:27:59 2015 Arthur Amstutz
// Last update Sat Jan 17 15:30:36 2015 Frédéric Ledarath
//

#ifndef OBJECT_HH_
# define OBJECT_HH_

#include <string>

class Object
{
protected:
  std::string	title;

public:
  Object(const std::string &t);
  virtual	~Object();
  virtual void	isTaken() const = 0;
  std::string *getTitle();
};

#endif
